package view;

import java.io.File;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.util.Arrays;
import java.util.HashMap;

import controller.Command;
import controller.Controller;

/**
 * The Class MyView.
 * 
 * @author Rea Bar and Tom Eileen Hirsch
 */
public class MyView implements View {

	/** The hash map. */
	protected HashMap<String, Command> hm;
	
	/** The controller. */
	protected Controller controller;
	
	/** The CLI. */
	protected CLI cli;
	
	
	/* (non-Javadoc)
	 * @see view.View#setController(controller.Controller)
	 */
	@Override
	public void setController(Controller controller) {
		this.controller = controller;
	}

	/**
	 * Instantiates a new my view.
	 *
	 * @param cli the CLI
	 */
	public MyView(CLI cli) {
		this.cli = cli;
		
	}
	
	/* (non-Javadoc)
	 * @see view.View#setHashMap(java.util.HashMap)
	 */
	@Override
	public void setHashMap(HashMap<String, Command> hashMap) {
		this.hm = hashMap;
		
	}

	/* (non-Javadoc)
	 * @see view.View#start()
	 */
	public void start() throws IOException{
		cli.setHashMap(hm);
		cli.start();
	}

	/* (non-Javadoc)
	 * @see view.View#dirPathCommand(java.lang.String)
	 */
	@Override
	public void dirPathCommand(String path){
		File file = new File(path);
		if(file.exists()){
			System.out.println("Files in this path: ");
			for(File f : file.listFiles())
				System.out.println(f.getName());
		}

		else{
			this.errorPrinting("The path <"+ path +"> does not exist");
		}

	}

	/* (non-Javadoc)
	 * @see view.View#MazeReady(java.lang.String)
	 */
	@Override
	public void MazeReady(String name) {
		System.out.println("maze <"+name+"> is ready");

	}

	/* (non-Javadoc)
	 * @see view.View#showMazeSize(int)
	 */
	@Override
	public void showMazeSize(int size) {
		System.out.println("Maze size in memory: "+ size);

	}

	/* (non-Javadoc)
	 * @see view.View#solutionReady(java.lang.String)
	 */
	@Override
	public void solutionReady(String name) {
		System.out.println("solution for <"+name+"> is ready");

	}

	/* (non-Javadoc)
	 * @see view.View#showSizeInFile(long)
	 */
	@Override
	public void showSizeInFile(long size) {
		System.out.println("Fize Size: "+ size);

	}

	/* (non-Javadoc)
	 * @see view.View#printMaze(byte[])
	 */
	@Override
	public void printMaze(byte[] mazeByteArray) {
		//print the maze
		int row = ByteBuffer.wrap(Arrays.copyOfRange(mazeByteArray, 24, 28)).getInt();
		int height = ByteBuffer.wrap(Arrays.copyOfRange(mazeByteArray, 28, 32)).getInt();
		int column = ByteBuffer.wrap(Arrays.copyOfRange(mazeByteArray, 32, 36)).getInt();
		int k = 36;
		for(int i=0;i<height;i++)
		{
			if(i!=0)
				System.out.println();
			for(int j=0;j<row;j++)
			{
				System.out.println();
				for(int g=0;g<column;g++)
				{
					//System.out.print(mazeByteArray[k]);
					System.out.print(ByteBuffer.wrap(Arrays.copyOfRange(mazeByteArray, k, (k+4))).getInt());
					k+=4;
				}
			}
		}
		System.out.println();
	}

	/* (non-Javadoc)
	 * @see view.View#DisplayCross(int[][])
	 */
	@Override
	public void DisplayCross(int[][] crossSectionBy) {
		//print the crossSection

		int t = crossSectionBy.length;
		int p = crossSectionBy[0].length;
		for(int tempT = 0;tempT<t;tempT++)
		{
			System.out.println();
			for(int tempP = 0;tempP<p;tempP++)
			{
				System.out.print(crossSectionBy[tempT][tempP]);
			}
		}

		System.out.println();
	}

	/* (non-Javadoc)
	 * @see view.View#getSolutionByteArray(java.lang.String)
	 */
	@Override
	public void getSolutionByteArray(String solution) {
		System.out.println(solution);

	}
	
	/* (non-Javadoc)
	 * @see view.View#errorPrinting(java.lang.String)
	 */
	@Override
	public void errorPrinting(String stringArray){
		System.out.println(stringArray);
	}



}

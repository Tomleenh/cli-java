package view;

import java.io.IOException;
import java.util.HashMap;

import controller.Command;
import controller.Controller;

/**
 * The Interface View.
 * 
 * @author Rea Bar and Tom Eileen Hirsch
 */
public interface View {
	
	/**
	 * Start.
	 *
	 * @throws IOException Signals that an I/O exception has occurred.
	 */
	void start() throws IOException;
	
	/**
	 * Error printing.
	 *
	 * @param stringArray the error info
	 */
	public void errorPrinting(String stringArray);
	/**
	 * Sets the hash map.
	 *
	 * @param hashMap the hash map
	 */
	void setHashMap(HashMap<String, Command> hashMap);
	
	/**
	 * Sets the controller.
	 *
	 * @param controller the new controller
	 */
	void setController(Controller controller);
	
	/**
	 * Dir path command.
	 *
	 * @param path the path
	 */
	void dirPathCommand(String path);
	
	/**
	 * Maze ready.
	 *
	 * @param name the name
	 */
	void MazeReady(String name);
	
	/**
	 * Show maze size.
	 *
	 * @param num the size
	 */
	void showMazeSize(int num);
	
	/**
	 * Solution ready.
	 *
	 * @param name the maze name
	 */
	void solutionReady(String name);
	
	/**
	 * Show size in file.
	 *
	 * @param size the size
	 */
	void showSizeInFile(long size);
	
	/**
	 * Prints the maze.
	 *
	 * @param mazeByteArray the maze byte array
	 */
	void printMaze(byte[] mazeByteArray);
	
	/**
	 * Display cross.
	 *
	 * @param crossSectionBy the cross section by
	 */
	void DisplayCross(int[][] crossSectionBy);
	
	/**
	 * Gets the solution byte array.
	 *
	 * @param solution the solution
	 * return the solution byte array
	 */
	void getSolutionByteArray(String solution);
	


}

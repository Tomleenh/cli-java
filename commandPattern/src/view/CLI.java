package view;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.HashMap;

import controller.Command;

/**
 * The Class CLI.
 * 
 * @author Rea Bar and Tom Eileen Hirsch
 */
public class CLI  implements Runnable{

	/** The running. */
	private volatile boolean running = true;

	/** The command hashMap. */
	private HashMap<String, Command> hm;

	/** The out - PrintWriter. */
	private PrintWriter out;

	/** The in - BufferedReader. */
	private BufferedReader in;

	/**
	 * Sets the hash map.
	 *
	 * @param hashMap the hash map
	 */
	public void setHashMap(HashMap<String, Command> hashMap){
		this.hm = hashMap;
	}

	/**
	 * Instantiates a new CLI.
	 *
	 * @param out the out
	 * @param in the in
	 */
	public CLI(PrintWriter out ,BufferedReader in) {
		//this.hm = hm;
		//out = new PrintWriter(fileName);
		//in = new BufferedReader(new FileReader(fileName));
		this.out = out;
		this.in = in;
	}



	/**
	 * Start.
	 * Receives commands from the user
	 */
	public void start(){
		this.run();
	}

	/* (non-Javadoc)
	 * @see java.lang.Runnable#run()
	 */
	@Override
	public void run() {
		try
		{
			String commandName = null;
			String[] args = null;
			String line;
			while (running)
			{
				System.out.println("Enter command: ");
				line = in.readLine();
				String[] splitLine = line.split(" ");
				commandName = splitLine[0];
				if (commandName.equals("display")){
					if(splitLine.length>2)
						commandName = splitLine[1];
				}
				switch(commandName){
				// Note that this is assuming valid input
				case "dir":
					args = new String[1];
					args[0] = splitLine[1];
					break;
				case "generate":
					args = new String[4];
					args[0] = splitLine[3];
					String ints = splitLine[4];
					String[] xyz = ints.split(",");
					for(int i = 1;i <4;i++)
					{
						args[i] = xyz[i-1];
					}
					break;
				case "display":
					args = new String[1];
					args[0] = splitLine[1];
					break;
				case "cross":
					args = new String[3];
					for(int i=0;i<2;i++){
						args[i] = splitLine[i+4];
					}
					args[2] = splitLine[7];
					break;
				case "save":
					args = new String[2];
					for(int i=0;i<2;i++){
						args[i] = splitLine[i+2];
					}
					break;
				case "load":
					args = new String[2];
					for(int i=0;i<2;i++){
						args[i] = splitLine[i+2];
					}
					break;
				case "maze":
					args = new String[1];
					args[0] = splitLine[2];
					break;
				case "file":
					args = new String[1];
					args[0] = splitLine[2];
					break;
				case "solve":
					args = new String[2];
					for(int i=0;i<2;i++){
						args[i] = splitLine[i+1];
					}
					break;
				case "solution":
					args = new String[1];
					args[0] = splitLine[2];
					break;
				case "exit":
					this.stopRunning();
					break;
				default: 
					System.out.println("There is no such command");
					break;
				}

				// select the command
				if(hm.containsKey(commandName)){
					Command command = hm.get(commandName);
					command.doCommand(args);
				}

			}
			System.out.println("A successful exit from the program");
		} 
		catch (IOException e) 
		{			
			e.printStackTrace();
		} 
		finally 
		{
			try 
			{
				in.close();
				out.close();
			} 
			catch (IOException e) 
			{				
				e.printStackTrace();
			}				
		}

	}

	/**
	 * Stop running.
	 */
	public void stopRunning(){
		running = false;
	}

}


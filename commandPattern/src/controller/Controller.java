package controller;

/**
 * The Interface Controller.
 * 
 * @author Rea Bar and Tom Eileen Hirsch
 */
public interface Controller {

	/**
	 * Maze is ready.
	 *
	 * @param name the name
	 */
	void mazeIsReady(String name);
	
	/**
	 * Error printing.
	 *
	 * @param string the error info
	 */
	void errorPrinting(String string);

	/**
	 * Size in memory.
	 *
	 * @param length the length
	 */
	void sizeInMemory(int length);

	/**
	 * Solution ready.
	 *
	 * @param name the name
	 */
	void solutionReady(String name);

	/**
	 * Pass solution model.
	 *
	 * @param string the string
	 */
	void passSolutionModel(String string);

	/**
	 * Pass maze model.
	 *
	 * @param mazeByteArray the maze byte array
	 */
	void passMazeModel(byte[] mazeByteArray);

	/**
	 * Send cross section.
	 *
	 * @param crossSectionBy the cross section by
	 */
	void sendCrossSection(int[][] crossSectionBy);

	/**
	 * Size in file.
	 *
	 * @param l the size
	 */
	void sizeInFile(long l);
	
	/**
	 * Start running.
	 */
	void startRunning();

}

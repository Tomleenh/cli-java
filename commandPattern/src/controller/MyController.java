package controller;

import java.io.IOException;
import java.util.HashMap;

import model.Model;
import view.View;

/**
 * The Class MyController.
 * 
 * @author Rea Bar and Tom Eileen Hirsch
 */
public class MyController implements Controller, Runnable{
	
	/** The view. */
	protected View view;
	
	/** The model. */
	protected Model model;
	
	/** The hashMp. */
	protected HashMap<String, Command> hm;

	
	/**
	 * Gets the view.
	 *
	 * @return the view
	 */
	public View getView() {
		return view;
	}

	/**
	 * Sets the view.
	 *
	 * @param view the new view
	 */
	public void setView(View view) {
		this.view = view;
	}

	/**
	 * Gets the model.
	 *
	 * @return the model
	 */
	public Model getModel() {
		return model;
	}

	/**
	 * Sets the model.
	 *
	 * @param model the new model
	 */
	public void setModel(Model model) {
		this.model = model;
	}



	/**
	 * Gets the hashMap.
	 *
	 * @return the hashMap
	 */
	public HashMap<String, Command> getHm() {
		return hm;
	}



	/**
	 * Instantiates a new my controller.
	 * Instantiates the command hash map
	 *
	 * @param view the view
	 * @param model the model
	 */
	public MyController(View view,  Model model) {
		this.view = view;
		this.model = model;
		this.hm = new HashMap<String,Command>();
		
		hm.put("dir", new DirCommand());
		hm.put("generate", new GenerateMaze3dCommand());
		hm.put("display", new DisplayCommand());
		hm.put("cross", new DisplayCrossSectionBy());
		hm.put("save", new SaveMazeCommand());
		hm.put("load", new LoadMazeCommand());
		hm.put("maze", new MazeSizeCommand());
		hm.put("file", new FileSizeCommand());
		hm.put("solve", new SolveCommand());
		hm.put("solution", new DisplaySolutionCommand());
		//hm.put("exit",new ExitCommand()); //

	}
	
	/**
	 * The Class DirCommand.
	 * dir path
	 * Displays the files and folders that are inside specific path in the computer's file system
	 */
	public class DirCommand implements Command {

		/* (non-Javadoc)
		 * @see controller.Command#doCommand(java.lang.String[])
		 */
		@Override
		public void doCommand(String[] path) {

			view.dirPathCommand(path[0]);	//?
		}

	}
	
	/**
	 * The Class DisplayCommand.
	 * display maze_name.
	 * display the maze_name maze.
	 */
	public class DisplayCommand implements Command {

		/* (non-Javadoc)
		 * @see controller.Command#doCommand(java.lang.String[])
		 */
		@Override
		public void doCommand(String[] name) {
			model.askForMaze(name[0]);
			
		}

	}
	
	/**
	 * The Class DisplayCrossSectionBy.
	 * display cross section by {X/Y/Z} index for maze_name.
	 * display the 2-dimensional maze for the right axis, for the right index, of the maze maze_name.
	 */
	public class DisplayCrossSectionBy implements Command {

		/* (non-Javadoc)
		 * @see controller.Command#doCommand(java.lang.String[])
		 */
		@Override
		public void doCommand(String[] args) {
			
			try {
				Integer.parseInt(args[1]);
				  
				model.getCrossSection(args[0], Integer.parseInt(args[1]) ,args[2]);
				}
			catch (NumberFormatException e) {
				  view.errorPrinting("Error in index");;
				 } 
			
			

		}

	}
	
	/**
	 * The Class DisplaySolutionCommand.
	 * display solution maze_name.
	 * display The solution moves for maze_name.
	 */
	public class DisplaySolutionCommand implements Command {
		
		/* (non-Javadoc)
		 * @see controller.Command#doCommand(java.lang.String[])
		 */
		@Override
		public void doCommand(String[] args) {
			model.askSolution(args[0]);

		}

	}
//	
//	/**
//	 * The Class ExitCommand.
//	 */
//	public class ExitCommand implements Command {
//
//		@Override
//		public void doCommand(String[] args) {
//			
//		}
//
//	}
	
	/**
	 * The Class FileSizeCommand.
	 * file size maze_name.
	 * display the size of maze maze_name in file.
	 */
	public class FileSizeCommand implements Command {

		/* (non-Javadoc)
		 * @see controller.Command#doCommand(java.lang.String[])
		 */
		@Override
		public void doCommand(String[] name) {
			model.showSizeInFile(name[0]);

		}

	}
	
	/**
	 * The Class GenerateMaze3dCommand.
	 * generate 3d maze maze_name {int x, int y, int z}.
	 * Create a maze with the maze_name.
	 * will notify when the maze in ready.
	 */
	public class GenerateMaze3dCommand implements Command{
		
		/* (non-Javadoc)
		 * @see controller.Command#doCommand(java.lang.String[])
		 */
		@Override
		public void doCommand(String[] args) {
			
			try {
				Integer.parseInt(args[1]);
				Integer.parseInt(args[2]);
				Integer.parseInt(args[3]);
				  
				model.generate3dMaze(args[0], Integer.parseInt(args[1]), Integer.parseInt(args[2]), Integer.parseInt(args[3]));
				}
			catch (NumberFormatException e) {
				  view.errorPrinting("Error in Dimensions");;
				 } 
			}
			
		}

	
	/**
	 * The Class LoadMazeCommand.
	 * load maze file_name maze_name.
	 * load a new maze from the File file_name and save it with the name maze_name.
	 */
	public class LoadMazeCommand implements Command {

		/* (non-Javadoc)
		 * @see controller.Command#doCommand(java.lang.String[])
		 */
		@Override
		public void doCommand(String[] args) throws IOException {
			
			model.loadMaze(args[0], args[1]);

		}

	}
	
	/**
	 * The Class SaveMazeCommand.
	 * save maze maze_name file_name.
	 * save the maze maze_name in a compressed File name file_name.
	 * 
	 */
	public class SaveMazeCommand implements Command {

		/* (non-Javadoc)
		 * @see controller.Command#doCommand(java.lang.String[])
		 */
		@Override
		public void doCommand(String[] args) throws IOException {

			model.saveCompressedMazed(args[0], args[1]);

		}

	}
	
	/**
	 * The Class SolveCommand.
	 * solve maze_name algorithm
	 * algorithm: BFS / AStarManhattan / AStarAir.
	 * solve the maze maze_name using the algorithm.
	 * will notify when the solution in ready.
	 */
	public class SolveCommand implements Command {

		/* (non-Javadoc)
		 * @see controller.Command#doCommand(java.lang.String[])
		 */
		@Override
		public void doCommand(String[] args) {
			
			model.solveMaze(args[0],args[1]);

		}

	}

	/**
	 * The Class MazeSizeCommand.
	 * maze size maze_name.
	 * display the size of maze maze_name in memory.
	 */
	public class MazeSizeCommand implements Command {

		/* (non-Javadoc)
		 * @see controller.Command#doCommand(java.lang.String[])
		 */
		@Override
		public void doCommand(String[] args) throws IOException {
			model.showMazeSize(args[0]);

		}

	}


	/* (non-Javadoc)
	 * @see controller.Controller#mazeIsReady(java.lang.String)
	 */
	@Override
	public void mazeIsReady(String name) {
		//send the message to the view
		view.MazeReady(name);
		
	}



	/* (non-Javadoc)
	 * @see controller.Controller#sizeInMemory(int)
	 */
	@Override
	public void sizeInMemory(int num) {
		view.showMazeSize(num);
		
	}



	/* (non-Javadoc)
	 * @see controller.Controller#solutionReady(java.lang.String)
	 */
	@Override
	public void solutionReady(String name) {
		view.solutionReady(name);
		
	}



	/* (non-Javadoc)
	 * @see controller.Controller#passSolutionModel(java.lang.String)
	 */
	@Override
	public void passSolutionModel(String solution) {
		view.getSolutionByteArray(solution);
		
	}



	/* (non-Javadoc)
	 * @see controller.Controller#passMazeModel(byte[])
	 */
	@Override
	public void passMazeModel(byte[] mazeByteArray) {
		view.printMaze(mazeByteArray);
		
	}



	/* (non-Javadoc)
	 * @see controller.Controller#sendCrossSection(int[][])
	 */
	@Override
	public void sendCrossSection(int[][] crossSectionBy) {
		view.DisplayCross(crossSectionBy); 
		
	}



	/* (non-Javadoc)
	 * @see controller.Controller#sizeInFile(long)
	 */
	@Override
	public void sizeInFile(long size) {
		view.showSizeInFile(size);
		
	}



	/* (non-Javadoc)
	 * @see java.lang.Runnable#run()
	 */
	@Override
	public void run() {
		view.setController(this);
		model.setController(this);
		view.setHashMap(this.getHm());
		try {
			view.start();
		} catch (IOException e) {
			e.printStackTrace();
		}
		
	}
	
	/* (non-Javadoc)
	 * @see controller.Controller#startRunning()
	 */
	@Override
	public void errorPrinting(String stringArray){
		view.errorPrinting(stringArray);
	}
	
	@Override
	public void startRunning() {
		view.setController(this);
		model.setController(this);
		view.setHashMap(this.getHm());
		try {
			view.start();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

}

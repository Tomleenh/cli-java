package boot;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.PrintWriter;

import controller.Controller;
import controller.MyController;
import model.Model;
import model.MyModel;
import view.CLI;
import view.MyView;
import view.View;

/**
 * The Class Run.
 * Contains the main method
 * 
 * @author Rea Bar and Tom Eileen Hirsch
 */
public class Run {
//
//	/**
//	 * This is the main method which runs the program.
//	 * 
//	 * @param args the arguments
//	 */
	public static void main(String[] args) {
		BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
		PrintWriter writer = new PrintWriter(System.out);
		Model model = new MyModel();
		CLI cli = new CLI(writer, reader);
		View view = new MyView(cli);
		Controller controller = new MyController(view, model);
		//Thread t = new Thread(new MyController(view, model));
		//t.start();
		
		controller.startRunning();
		

	}

}

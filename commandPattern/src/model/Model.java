package model;

import java.io.FileNotFoundException;
import java.io.IOException;

import controller.Controller;

/**
 * The Interface Model.
 * 
 * @author Rea Bar and Tom Eileen Hirsch
 */
public interface Model {

	/**
	 * Sets the controller.
	 *
	 * @param controller the new controller
	 */
	void setController(Controller controller);
	
	/**
	 * Generate3d maze.
	 *
	 * @param args the arguments.
	 * @param x the x
	 * @param y the y
	 * @param z the z
	 */
	void generate3dMaze(String args, int x, int y,int z);

	/**
	 * Save compressed mazed.
	 *
	 * @param name the maze name
	 * @param fileName the File name
	 * @throws FileNotFoundException the file not found exception
	 * @throws IOException Signals that an I/O exception has occurred.
	 */
	void saveCompressedMazed(String name, String fileName) throws FileNotFoundException, IOException;

	/**
	 * Load maze.
	 *
	 * @param name the maze name
	 * @param fileName the File name
	 * @throws IOException Signals that an I/O exception has occurred.
	 */
	void loadMaze(String name, String fileName) throws IOException;

	/**
	 * Solve maze.
	 *
	 * @param name the maze name
	 * @param algorithm the algorithm
	 */
	void solveMaze(String name, String algorithm);

	/**
	 * Ask solution.
	 *
	 * @param string the string
	 */
	void askSolution(String string);

	/**
	 * Show maze size.
	 *
	 * @param name the maze name
	 * @throws IOException Signals that an I/O exception has occurred.
	 */
	void showMazeSize(String name) throws IOException;

	/**
	 * Ask for maze.
	 *
	 * @param name the maze name
	 */
	void askForMaze(String name);

	/**
	 * Gets the cross section.
	 *
	 * @param axis the axis
	 * @param parseInt the index
	 * @param name the maze name
	 * return the cross section
	 */
	void getCrossSection(String axis, int parseInt, String name);

	/**
	 * Show size in file.
	 *
	 * @param string the string
	 */
	void showSizeInFile(String string);

}

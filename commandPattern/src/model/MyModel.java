package model;


import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.ByteBuffer;
import java.util.Arrays;
import java.util.HashMap;

import algorithms.demo.Maze3dSearchableAdapter;
import algorithms.mazeGenerators.Maze3d;
import algorithms.mazeGenerators.MyMaze3dGenerator;
import algorithms.mazeGenerators.Position;
import algorithms.search.AStarAlgorithm;
import algorithms.search.BFSAlgorithm;
import algorithms.search.MazeAirDistance;
import algorithms.search.MazeManhattanDistance;
import algorithms.search.Searcher;
import algorithms.search.Solution;
import controller.Controller;
import io.MyCompressorOutputStream;
import io.MyDecompressorInputStream;

/**
 * The Class MyModel.
 * 
 * @author Rea Bar and Tom Eileen Hirsch
 */
public class MyModel implements Model {

	/** The solutions. */
	protected HashMap<String, Solution<Position>> solutions = new HashMap<>();
	
	/** The controller. */
	protected Controller controller;
	
	/** The maze hash map. */
	protected HashMap<String, Maze3d> mazeHashMap = new HashMap<>();
	
	/** The compressed maze. */
	protected HashMap<String, String> compressedMaze = new HashMap<>();

	/**
	 * Instantiates a new my model.
	 */
	public MyModel() {
	}

	/**
	 * Instantiates a new my model.
	 *
	 * @param controller the controller
	 */
	public MyModel(Controller controller) {
		this.controller = controller;
	}

	/* (non-Javadoc)
	 * @see model.Model#generate3dMaze(java.lang.String, int, int, int)
	 */
	@Override
	public void generate3dMaze(String name, int x, int y, int z){

		if(!mazeHashMap.containsKey(name)){
			// Generate(x,y,z) and save the maze as "name" in hashMap
			new Thread(new Runnable() {
				@Override
				public void run() {
					Maze3d maze = new MyMaze3dGenerator().generate(x, y, z);

					mazeHashMap.put(name, maze); 
					//send a message to the view via the controller that the maze is ready
					controller.mazeIsReady(name);
				}
			}).start();
		}
		else{
			controller.errorPrinting("please enter another name, this name already exists");
		}
	}


	/* (non-Javadoc)
	 * @see model.Model#saveCompressedMazed(java.lang.String, java.lang.String)
	 */
	@Override
	public void saveCompressedMazed(String name, String fileName) throws IOException {
		File f = new File(fileName);
		if(mazeHashMap.containsKey(name) && !f.exists()){
			OutputStream out = new MyCompressorOutputStream(new FileOutputStream(fileName));
			out.write(mazeHashMap.get(name).toByteArray());
			out.flush();
			out.close();
			compressedMaze.put(name, fileName);
		}

		else if(!mazeHashMap.containsKey(name)){
			controller.errorPrinting("The maze <" + name + "> does not exist");
		}

		else{
			controller.errorPrinting("The file name <" + fileName + "> already exists, please choose another file name");
		}
	}


	/* (non-Javadoc)
	 * @see model.Model#loadMaze(java.lang.String, java.lang.String)
	 */
	@Override
	public void loadMaze(String fileName, String name) throws IOException {
		File f = new File(fileName);
		if(f.exists() && !mazeHashMap.containsKey(name)){
			InputStream in = new MyDecompressorInputStream(new FileInputStream(fileName));
			byte b[] = new byte[36];
			in.read(b);
			int row = ByteBuffer.wrap(Arrays.copyOfRange(b, 24, 28)).getInt();
			int height = ByteBuffer.wrap(Arrays.copyOfRange(b, 28, 32)).getInt();
			int column = ByteBuffer.wrap(Arrays.copyOfRange(b, 32, 36)).getInt();
			byte byteArray[] = new byte[row*height*column*4];
			in.read(byteArray);
			in.close();
			byte[] combined = new byte[b.length + byteArray.length];
			System.arraycopy(b, 0, combined, 0, b.length);
			System.arraycopy(byteArray, 0, combined, b.length, byteArray.length);

			Maze3d loaded = new Maze3d(combined);
			mazeHashMap.put(name, loaded);
			compressedMaze.put(name, fileName);
		}

		else if(!f.exists()){
			controller.errorPrinting("The fileName <" + fileName + "> does not exist");
		}

		else if(mazeHashMap.containsKey(name)){
			controller.errorPrinting("please enter another name, this name <" + name + "> already exists");
		}
	}


	/* (non-Javadoc)
	 * @see model.Model#solveMaze(java.lang.String, java.lang.String)
	 */
	@Override
	public void solveMaze(String name, String algorithm) {

		if(mazeHashMap.containsKey(name)){
			new Thread(new Runnable() {

				@Override
				public void run() {
					Solution<Position> sol = new Solution<Position>();
					Searcher<Position> searcher = null;
					if(algorithm.contains("BFS") || algorithm.contains("AStarManhattan") || algorithm.contains("AStarAir")){
						if (algorithm.contains("BFS")){
							searcher = new BFSAlgorithm<>();
						}
						else if(algorithm.contains("AStar")){
							if(algorithm.contains("Manhattan"))
								searcher= new AStarAlgorithm<Position>(new MazeManhattanDistance());
							else if (algorithm.contains("Air"))
								searcher = new AStarAlgorithm<Position>(new MazeAirDistance());
						}
						
						sol = searcher.search(new Maze3dSearchableAdapter(mazeHashMap.get(name)));
						saveSolutions(name,sol); // save the solution in the model
						controller.solutionReady(name);
					}
					else
						controller.errorPrinting("Wrong Algorithm");					
				}
			}).start();

		}

		else if(!mazeHashMap.containsKey(name)){
			controller.errorPrinting("the maze <" + name + "> does not exist");
		}
	}


	/**
	 * Save solutions.
	 *
	 * @param name the maze name
	 * @param sol the solution
	 */
	private void saveSolutions(String name, Solution<Position> sol) {
		if(mazeHashMap.containsKey(name)){
			solutions.put(name, sol);	
		}
		else{
			controller.errorPrinting("the maze <" + name + "> does not exist");
		}
	}


	/* (non-Javadoc)
	 * @see model.Model#askSolution(java.lang.String)
	 */
	@Override
	public void askSolution(String name) {

		if(solutions.containsKey(name)){
			controller.passSolutionModel(solutions.get(name).toString());
		}

		else{
			controller.errorPrinting("the maze <" + name + "> was not solved yet, please solve first");
		}
	}


	/* (non-Javadoc)
	 * @see model.Model#showMazeSize(java.lang.String)
	 */
	@Override
	public void showMazeSize(String name) throws IOException {
		if(mazeHashMap.containsKey(name)){
			controller.sizeInMemory(mazeHashMap.get(name).toByteArray().length);
		}
		
		else{
			controller.errorPrinting("the maze <" + name + "> does not exist");
		}
	}

	/* (non-Javadoc)
	 * @see model.Model#askForMaze(java.lang.String)
	 */
	@Override
	public void askForMaze(String name) {
		if(mazeHashMap.containsKey(name)){
			try {
				byte[] mazeByteArray = mazeHashMap.get(name).toByteArray();
				controller.passMazeModel(mazeByteArray);	
			} catch (IOException e) {
				e.printStackTrace();
				
			}
		}
		else
			controller.errorPrinting("Error in maze name <"+name+">");
	}

	/* (non-Javadoc)
	 * @see model.Model#getCrossSection(java.lang.String, int, java.lang.String)
	 */
	@Override
	public void getCrossSection(String by, int parseInt, String name) {
				
			if(mazeHashMap.containsKey(name)){
				if (by.equals("X")){
					try{
						mazeHashMap.get(name).getCrossSectionByX(parseInt);
						
						controller.sendCrossSection(mazeHashMap.get(name).getCrossSectionByX(parseInt));
					}
				catch (IndexOutOfBoundsException e) {
					controller.errorPrinting("Index Out Of Bounds");
				}
				}
				else if (by.equals("Y")){
					try{
						mazeHashMap.get(name).getCrossSectionByY(parseInt);
						
						controller.sendCrossSection(mazeHashMap.get(name).getCrossSectionByY(parseInt));
					}
				catch (IndexOutOfBoundsException e) {
					controller.errorPrinting("Index Out Of Bounds");
				}
				}
				else if (by.equals("Z")){
					try{
						mazeHashMap.get(name).getCrossSectionByZ(parseInt);
						
						controller.sendCrossSection(mazeHashMap.get(name).getCrossSectionByZ(parseInt));
					}
				catch (IndexOutOfBoundsException e) {
					controller.errorPrinting("Index Out Of Bounds");
				}
				}
				else 
					controller.errorPrinting("Error in axis");
			}
			else {
			controller.errorPrinting("Error in maze name <"+name+">");
		}
				
	}

	/* (non-Javadoc)
	 * @see model.Model#showSizeInFile(java.lang.String)
	 */
	@Override
	public void showSizeInFile(String name) {

		if(compressedMaze.containsKey(name)){
			File f = new File(compressedMaze.get(name));
			controller.sizeInFile(f.length());
		}
	}

	/* (non-Javadoc)
	 * @see model.Model#setController(controller.Controller)
	 */
	@Override
	public void setController(Controller controller) {
		this.controller = controller;
	}
}
